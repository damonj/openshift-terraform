OpenShift Terraform using LibVirt

You will need the qcow 2 file downloaded locally or you'll be doing a lot of downloading.

1. Clone repo
2. Create a terraform.tfvars file to set the variables .e.g

#Mandatory
cluster = "Jacob"
subnet = "192.168.0.0/24"
ssh-key = "<my ssh public key>"

#Optional overrides
memory = "4096"
vcpu   = 2
master-nodes = 1
app-nodes = 2
infra-nodes = 2


4. run terraform init to pull in required plugins
5. run terraform apply, confirm the plan and wait
6. run ./create_files.py to genrate files for ansible to distribute
7. run ansible-playbook -i ansible.hosts pre-prep.yaml
8. ssh into the master server
9. run the openshift prereq playbook etc..
