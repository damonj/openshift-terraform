###################### DISKS #################


resource "libvirt_volume" "openshift-app-boot" {
  count = "${var.app-nodes}"
  name = "${var.cluster}-app-boot-${count.index}"
  pool = "default"
  #source = "https://cloud.centos.org/centos/7/images/CentOS-7-x86_64-GenericCloud.qcow2"
  source = "/root/disk-images/CentOS-7-x86_64-GenericCloud.qcow2"
  format = "qcow2"
}

resource "libvirt_volume" "openshift-app-docker" {
  count = "${var.app-nodes}"
  name = "${var.cluster}-app-docker-${count.index}.qcow2"
  format = "qcow2"
  pool = "default"
  size           = "${var.disk-size}"
}

##################### DOMAINS ######################
resource "libvirt_domain" "openshift-app" {
  count = "${var.app-nodes}"
  name   = "${var.cluster}-openshift-app-${count.index}"
  memory = "${var.memory}"
  vcpu   = "${var.vcpu}"


  network_interface {
    network_name = "${var.cluster}-openshift"
    hostname   = "${var.cluster}-openshift-app-${count.index}"
    wait_for_lease = true
    #bridge = "br0"
  }

  disk {
    volume_id = "${libvirt_volume.openshift-app-boot[count.index].id}"
  }

  disk {
    volume_id = "${libvirt_volume.openshift-app-docker[count.index].id}"
  }

  cloudinit = "${libvirt_cloudinit_disk.commoninit.id}"

  console {
    type = "pty"
    target_type = "serial"
    target_port = "0"
  }

  graphics {
    type = "spice"
    listen_type = "address"
    autoport = true
  }
}

