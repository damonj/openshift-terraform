variable "cluster" {
  type = string
  #default = "default"
}

variable "ssh-key" {
  type = string
}

variable "dns-domain" {
  type = string
}

variable "subnet" {
  type = string
}

variable "memory" {
  type = number
  default = 4096
}

variable "vcpu" {
   type = number
   default = 2
}

variable "master-nodes" {
  type = number
  default = 1
}

variable "app-nodes" {
  type = number
  default = 2
}

variable "infra-nodes" {
  type = number
  default = 2
}

variable "disk-size" {
  type = number
  default = 10737418240
}
