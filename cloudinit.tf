data "template_file" "user_data" {
  template = "${file("${path.module}/cloud_init.cfg")}"
  
  vars = {
     ssh-key = "${var.ssh-key}"
  }
}

# Use CloudInit to add the instance
resource "libvirt_cloudinit_disk" "commoninit" {
  name = "commoninit-${var.cluster}.iso"
  user_data      = "${data.template_file.user_data.rendered}"
}
