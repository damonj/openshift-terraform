resource "libvirt_network" "openshift_network" {
  # the name used by libvirt
  name = "${var.cluster}-openshift"
  # mode can be: "nat" (default), "none", "route", "bridge"
  mode = "nat"
  #  the domain used by the DNS server in this network
  domain = "${var.dns-domain}"
  addresses = ["${var.subnet}"]

  # mtu = 9000

  dns {
    enabled = true
    local_only = false
  }


}
