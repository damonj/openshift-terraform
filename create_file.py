#!/usr/bin/python
import json
from jinja2 import Environment, FileSystemLoader

file_loader = FileSystemLoader('templates')
env = Environment(loader=file_loader)

#get this programattically from the network
domain = "example.local"


with open('terraform.tfstate') as f:
    d = json.load(f)
    #print(d)


resources = d['resources']
#print(type(resources[0]))

masters = list()
appnodes = list()
infranodes = list()
networks = dict()

domains = [ x for x in resources if x['type'] == 'libvirt_domain']
nets = [ x for x in resources if x['type'] == 'libvirt_network']
for network in nets:
    if network['name'] == 'openshift_network':
        
        networks['domain'] = network['instances'][0]['attributes']['domain']
        networks['addresses'] = network['instances'][0]['attributes']['addresses'][0]
        tmp_net = network['instances'][0]['attributes']['addresses'][0]
        tmp_net2= tmp_net.split('.')
        networks['gateway'] = "%s.1" %'.'.join(tmp_net2[0:3])



#print(domains[0])
for m in domains:
    instances = m['instances']
    if "infra" in m['name']:
        count = 0
        for server in m['instances']:
            tmp = {}
            tmp['hostname'] = "infra%s" % count
            tmp['fqdn'] = "infra%s.%s" % (count, networks['domain'])
            tmp['ip'] = server['attributes']['network_interface'][0]['addresses'][0]
            infranodes.append(tmp)
            count += 1


    if "app" in m['name']:
        count = 0
        for server in m['instances']:
            tmp = {}
            tmp['hostname'] = "apps%s" % count
            tmp['fqdn'] = "apps%s.%s" % (count, networks['domain'])
            tmp['ip'] = server['attributes']['network_interface'][0]['addresses'][0]
            appnodes.append(tmp)
            count += 1

    if "master" in m['name']:
        count = 0
        for server in m['instances']:
            tmp = {}
            tmp['hostname'] = "master%s" % count
            tmp['fqdn'] = "master%s.%s" % (count, networks['domain'])
            tmp['ip'] = server['attributes']['network_interface'][0]['addresses'][0]
            masters.append(tmp)
            count += 1

data = {}
data['master_nodes'] = masters
data['infra_nodes'] = infranodes
data['app_nodes'] = appnodes
data['network'] = networks


# Ansible hosts for pre config
template = env.get_template('ansible-inventory.j2')
output = template.render(data=data)
f = open("ansible-inventory.hosts", "w")
f.write(output)
f.close();
print("Ansible : Inventory File Created")


#/etc/hosts
template = env.get_template('hosts.j2')
output = template.render(data=data)
f = open("files/hosts", "w")
f.write(output)
f.close();
print("Ansible : Hosts File Created")


#/etc/resolve.conf
template = env.get_template('resolv.conf.j2')
output = template.render(data=data)
f = open("files/resolv.conf", "w")
f.write(output)
f.close();
print("Ansible : resolv.conf File Created")

#openshift-ansible inventory
template = env.get_template('inventory.hosts.j2')
output = template.render(data=data)
f = open("files/openshift-ansible-inventory.hosts", "w")
f.write(output)
f.close();
print("Ansible : openshift-ansible-inventory.hosts File Created")


#dnsmasq.conf
template = env.get_template('dnsmasq.conf.j2')
output = template.render(data=data)
f = open("files/dnsmasq.conf", "w")
f.write(output)
f.close();
print("Ansible : dnsmasq.conf File Created")


#resolve.dnsmasq
template = env.get_template('resolv.dnsmasq.j2')
output = template.render(data=data)
f = open("files/resolv.dnsmasq", "w")
f.write(output)
f.close();
print("Ansible : resolv.dnsmasq File Created")

#origin-upstream-dns.conf.j2
template = env.get_template('origin-upstream-dns.conf.j2')
output = template.render(data=data)
f = open("files/origin-upstream-dns.conf", "w")
f.write(output)
f.close();
print("Ansible : origin-upstream-dns.conf File Created")






